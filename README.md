This script extracts audio from an MKV and splits them according to chapters, encoding the audio in FLAC format (default compression level 5).

## Requirements

The script requires ```ffmpeg``` to be installed.

## Example

chapter_names.json:
```
[
	"Chapter 1",
	"Chapter 2"
	"Chapter 3"
]
```

To run the script:
```
python3 extract.py -i concert.mkv -c chapter_names.json -o out
```