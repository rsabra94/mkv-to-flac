import os
import sys
import json
import argparse
from queue import Queue
from threading import Thread
from pathlib import Path
from subprocess import Popen, PIPE
from multiprocessing import cpu_count

FFPROBE_FILEPATH = "ffprobe"
NUM_WORKER_THREADS = cpu_count()

def work(time_queue):
	while True:
		idx, chapter_name, start_time, end_time = time_queue.get()
		if idx is None:
			break
		duration = end_time - start_time
		output_filename = "{:02d} - {}.flac".format(idx, chapter_name)
		output_filepath = Path(output_dir, output_filename)
		args = ["ffmpeg", "-i", str(filepath), "-ss", str(start_time), "-t", str(duration), "-c:a", "flac", str(output_filepath)]
		print("Processing chapter {:02d} - {} ...".format(idx, chapter_name))
		process = Popen(args, stdout=PIPE, stderr=PIPE)
		process.wait()
		try:
			assert process.returncode == 0
		except AssertionError:
			print(process.stderr.read().decode('utf-8'))

parser = argparse.ArgumentParser(
	description="Extracts audio chapters from MKV and converts them to FLAC in parallel"
)
parser.add_argument("-i", "--input", type=str, required=True, help="Input filepath")
parser.add_argument("-c", "--chapters", type=str, required=True, help="Chapter names JSON filepath")
parser.add_argument("-o", "--output", type=str, required=True, help="Output directory")

args = parser.parse_args()

filepath = args.input
chapter_names_json_filepath = args.chapters
output_dir = args.output

try:
	os.mkdir(str(output_dir))
except OSError:
	pass

args = [FFPROBE_FILEPATH, "-i", str(filepath), "-print_format", "json", "-show_chapters"]
process = Popen(args, stdout=PIPE, stderr=PIPE)
process.wait()
chapters_json_str = process.stdout.read().decode('utf-8')
chapters_json = json.loads(chapters_json_str)

with open(str(chapter_names_json_filepath)) as chapter_names_json_file:
	chapter_names = json.load(chapter_names_json_file)

chapters = chapters_json["chapters"]

time_queue = Queue()
for i in range(len(chapters)):
	chapter = chapters[i]
	chapter_name = chapter_names[i]
	start_time = float(chapter["start_time"])
	end_time = float(chapter["end_time"])
	time_queue.put((i + 1, chapter_name, start_time, end_time))

for _ in range(NUM_WORKER_THREADS):
	time_queue.put((None, None, None, None))

worker_threads = [Thread(target=work, args=[time_queue]) for _ in range(NUM_WORKER_THREADS)]
for worker_thread in worker_threads:
	worker_thread.start()

for worker_thread in worker_threads:
	worker_thread.join()